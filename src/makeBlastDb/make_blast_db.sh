#!/usr/bin/env bash
set -e
POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
      -h|--help)
      echo "Builds BLAST db"
      echo ""
      echo "Usage"
      echo "-i <inputDir> -o <outputDir> <version>"
      echo "-i|--input-dir: Path to input directory"
      echo "-o|--output-dir: Path to output directory"
      echo "--db: DB name"
      echo "version: DB version (without 'V')"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
      -i|--input-dir)
      INPUT_DIR=$2
      shift
      shift
      ;;
      -fa|--fasta-assembly)
      FA=$2
      shift
      shift
      ;;
      -fna|--fasta-nucleotides)
      FNA=$2
      shift
      shift
      ;;
      -faa|--fasta-proteins)
      FAA=$2
      shift
      shift
      ;;
      -o|--output-dir)
      OUTPUT_DIR=$2
      shift
      shift
      ;;
      --db)
            DB_NAME=$2
            shift
            shift
            ;;
      *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

if [[ -z ${POSITIONAL[0]} ]]; then
  echo "Please provide which db version to build"
  exit 1
else
  VERSION=${POSITIONAL[0]}
fi

mkdir -p $OUTPUT_DIR
rm -rf $OUTPUT_DIR/*

set -x
/usr/local/lib/blast+/bin/makeblastdb -in $INPUT_DIR/${DB_NAME}_${VERSION}_assembly.fa -out $OUTPUT_DIR/${DB_NAME}_${VERSION}_assembly -title "${DB_NAME} $VERSION -- Genome assemblies" -dbtype nucl -parse_seqids
/usr/local/lib/blast+/bin/makeblastdb -in $INPUT_DIR/${DB_NAME}_${VERSION}_cds.fna -out $OUTPUT_DIR/${DB_NAME}_${VERSION}_cds -title "${DB_NAME} $VERSION -- CDS Nucleotide" -dbtype nucl -parse_seqids
/usr/local/lib/blast+/bin/makeblastdb -in $INPUT_DIR/${DB_NAME}_${VERSION}_protein.faa -out $OUTPUT_DIR/${DB_NAME}_${VERSION}_protein -title "${DB_NAME} $VERSION -- CDS Protein" -dbtype prot -parse_seqids
