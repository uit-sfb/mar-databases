# MAR Databases

CI/CD pipeline for submission of samples to the [MAR databases](https://mmp2.sfb.uit.no/):
- [MarRef](https://mmp2.sfb.uit.no/marref/)
- [MarDB](https://mmp2.sfb.uit.no/mardb/)
- [MarFun](https://mmp2.sfb.uit.no/marfun/)
- [SalDB](https://mmp2.sfb.uit.no/saldb/)

MAR Databases are based on [CBF](https://gitlab.com/uit-sfb/cbf) framework whose endpoints are:
- [API](https://databasesapi.sfb.uit.no/)
- [UI](https://datawidgets.sfb.uit.no/)

For more information, see [here](https://gitlab.com/uit-sfb/cbf/-/wikis/home).

## Pipeline overview

TODO: diagram here

## Repository overview

- .gitlab-ci.yml is the entrypoint for the Gitlab CI/CD pipeline
- /pipeline contains the CI/CD pipeline stages definition
- /src contains some custom tools used by the pipeline
- /deployment contains docker-compose specs for the product services
- /dbs contains a manual backup of the datasets definitions
- /test contains som data for testing purpose