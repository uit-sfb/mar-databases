#In Cron:
#0 3 * * * /root/backup_mar_portal.sh

COPY_PARENT_DIR=/data/disk1/production_backups/backup-mar_portal
COPY_DIR=$COPY_PARENT_DIR/$(date +%F)
mkdir -p $COPY_DIR
cp -a /data/production/mar-databases $COPY_DIR

#Remove any directory older than 14 days
find $COPY_PARENT_DIR/* -type d -ctime +14 -exec rm -rf {} \;

