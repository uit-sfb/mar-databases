require 'erb'

# Cf https://raw.githubusercontent.com/wurmlab/sequenceserver/master/lib/sequenceserver/links.rb
# The links defined here are added to the ones defined in the original links.rb above

module SequenceServer
  # Module to contain methods for generating sequence retrieval links.
  module Links
    # Provide a method to URL encode _query parameters_. See [1].
    include ERB::Util
    alias encode url_encode

    MAR_DB_PATTERN = /\[mmpid=mmp\.db:(MMP\d+)\]/
    MAR_REF_PATTERN = /\[mmpid=mmp\.ref:(MMP\d+)\]/
    MAR_FUN_PATTERN = /\[mmpid=mmp\.fun:(MMP\d+)\]/
    SAL_DB_PATTERN = /\[mmpid=mmp\.saldb:(MMP\d+)\]/

    def mardb
      return nil unless title.match(MAR_DB_PATTERN)
      acc = Regexp.last_match[1]
      acc = encode acc
      url = "https://mmp2.sfb.uit.no/MarDB/records/#{acc}"
      {
        order: 2,
        title: 'MarDB',
        url:   url,
        icon:  'fa-external-link'
      }
    end

    def marref
          return nil unless title.match(MAR_REF_PATTERN)
          acc = Regexp.last_match[1]
          acc = encode acc
          url = "https://mmp2.sfb.uit.no/MarRef/records/#{acc}"
          {
            order: 2,
            title: 'MarRef',
            url:   url,
            icon:  'fa-external-link'
          }
        end

    def marfun
          return nil unless title.match(MAR_FUN_PATTERN)
          acc = Regexp.last_match[1]
          acc = encode acc
          url = "https://mmp2.sfb.uit.no/MarFun/records/#{acc}"
          {
            order: 2,
            title: 'MarFun',
            url:   url,
            icon:  'fa-external-link'
          }
        end

    def saldb
          return nil unless title.match(SAL_DB_PATTERN)
          acc = Regexp.last_match[1]
          acc = encode acc
          url = "https://mmp2.sfb.uit.no/SalDB/records/#{acc}"
          {
            order: 2,
            title: 'SalDB',
            url:   url,
            icon:  'fa-external-link'
          }
        end
  end
end

# [1]: https://stackoverflow.com/questions/2824126/whats-the-difference-between-uri-escape-and-cgi-escape