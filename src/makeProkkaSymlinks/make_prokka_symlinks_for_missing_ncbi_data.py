#!/usr/bin/env python3

# Usage: ./make_prokka_symlinks_for_missing_ncbi_data.py <genomesdir>
# (genomesdir ex: /srv/mar/MarDB/genomes/)

import os, sys
symlinked = 0
existing = 0
ncbi = 0
verbose = True

for root, dirs, files in os.walk(sys.argv[1]):
        if root.endswith("genomes"):
                continue
        if os.path.split(root)[-1].startswith("GCA"):

                if ("prokka" in dirs) and ("protein.faa" not in files) and ("cds.fna" not in files):
                        symlinked+=1
                        dstprotein=os.path.join(root, "protein.faa")
                        dstcds=os.path.join(root, "cds.fna")
                        srcprotein=os.path.join(os.path.join(root, "prokka"), os.path.split(root)[-1]+'.faa')
                        srccds=os.path.join(os.path.join(root, "prokka"), os.path.split(root)[-1]+'.fna')
                        os.symlink(srcprotein, dstprotein)
                        os.symlink(srccds, dstcds)
                elif ("prokka" in dirs) and (os.path.islink(os.path.join(root, "protein.faa")) and (os.path.islink(os.path.join(root, "cds.fna")))):
                        existing+=1
                else:
                        ncbi+=1

if verbose == True:
        print ("Created symlinks for {} entries (Prokka)\nIgnored {} entries (already existing symlinks)\nIgnored {} entries (has NCBI seqdata or missing Prokka seqdata)\nTotal entries processed {}". format(symlinked, existing, ncbi, (symlinked+existing+ncbi)))

