#!/usr/bin/env bash

set -e

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
      -h|--help)
      echo "Publish file to artifactory"
      echo ""
      echo "Usage"
      echo "--file <pathToFile>"
      echo "--artifact <artifactName>"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
      --file)
      FILE=$2
      shift
      shift
      ;;
      --artifact)
      ARTIFNAME=$2
      shift
      shift
      ;;
      *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

CREDS="${HOME}/.sbt/.credentials"
HOST=$(cat "${CREDS}" | sed -n -e '/^host=/p' | sed -e "s/^host=//")
USER=$(cat "${CREDS}" | sed -n -e '/^user=/p' | sed -e "s/^user=//")
PASSWORD=$(cat "${CREDS}" | sed -n -e '/^password=/p' | sed -e "s/^password=//")
URL="https://$HOST/artifactory/generic-local/no.uit.sfb/${ARTIFNAME}"
echo "Publishing to ${URL}"
RET=$(echo $(curl -u ${USER}:${PASSWORD} --output /dev/null --write-out "%{http_code}" -T ${FILE} ${URL}) | tr -d '\n' | tr -d '"')
if [[ $(echo "$RET" | tail -n 1) != 201 ]]; then echo "Returned status code ${RET}"; exit 1; else echo "Success"; fi;