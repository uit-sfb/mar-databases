# Wordpress deployment

The wordpress deployment is a manual job (one needs to click on the run button to start it).
The reason for that is that it should be started only once and there is no reason to have to re-run the job (except maybe if somehow the container gets stuck).

The attached script is meant to be added to Cron (see instructions inside the script). This operation should be performed manually.
