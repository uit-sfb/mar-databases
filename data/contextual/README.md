## Directory content

The curation team commits here the following files:

- `<DbName>/accession_list.tsv`
- `<DbName>/release_genome_meta.tsv`
- ~~entries.tsv~~


### accession_list.tsv

The accession_list.tsv contains 1 column consisting of the genbank accession of the new entries to add to the upcoming version.
Example below:
```
genbank_accession
MN908947.3
MT007544.1
MT126808.1
MT093631.2
...
```

### release_genome_meta.tsv

NBCI contextual data manually fetched by curator.

### ~~entries.tsv~~

`entries.tsv` contains new or updated entries using [this format](https://gitlab.com/uit-sfb/cbf/-/wikis/Data/TSV-format).
It used to be committed in this directory, but is now instead uploaded directly to the UI.

#### Deleting an entry

If an entry is to be removed from the database, it should appear in `entries.tsv` with its id prefixed with `!`.
For instance:
```
...
!covid19:SFB_COVID19_MT007544
...
```

## Workflow

1. Create a new branch (from `master`).
2. Add `accession_list.tsv` and `release_genome_meta.tsv` and commit.
   A pipeline is triggered and, if successful, generates an artifact in stage `multifetch` called `metadata.tsv`.
   This file is the output of Multifetch.
3. Download artifact `metadata.tsv` and curate it.
4. ~~Copy your curated version into `entries.tsv` (without prepending the whole database!!) and commit.
   A pipeline is triggered which validates each record and push the successful ones (see below for how to deal with failures).~~  
   Download the curated tsv file from OpenRefine and upload it directly via the UI. Follow the instructions given there.
5. Copy data/contextual/who/variants.tsv from the previous release and update it as per https://www.who.int/en/activities/tracking-SARS-CoV-2-variants/.
6. When curators agree to do a release, click on the run button of the `keys` stage and check that the rest of the pipeline executes without failure.
7. Check on the portal that the number of records has been updated (you need to reload the page to see the changes).

Note: Once a version has been released, it is not possible to update entries or releasing again.
